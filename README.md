# ioBridge

This program runs on an Adafruit Feather, connected to an Ethernet Featherwing.

It monitors 4 inputs (eg, switch, button, relay contacts etc) and when a state
change is detected, it sends a UDP packet to the specified IP address.

It's purpose is to interface physical equipment (ie, studio console, phone pabx,
doorbell, etc) to the "OnAirScreen" software that provides our in-studio Heads
Up Display (HUD).

A status LED provides rudimentary detail about current operation:

| LED State      | Meaning |
| -------------- | ------- |
| OFF            | No power, or initialization not complete. |
| ON             | Working OK. |
| SINGLE BLINK   | State change on input detected (UDP packet should be sent). |
| FLASHING TWICE | No ethernet connection detected. |

Hardware Built and Code Written by Phillip Smith, Febuary 2020
