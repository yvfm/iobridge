/*
 Refer to README.md for full details
*/

#include <Bounce2.h>
#include <Ethernet.h>
#include <EthernetUdp.h>

/******************************************************************************
 * CONFIGURATION CONSTANTS
 *****************************************************************************/
const bool ENABLE_DEBUG = true;     // enables debug messages on console output
const int DEBOUNCE_INTERVAL = 100;  // milliseconds for input debounce detection
const int STATUS_LED_PIN = 13;      // pin used for the status led light
const int RELAY_CONTROL_PIN = 18;   // pin used to control the relay linked to input #0
const int NUMBER_OF_INPUTS = 4;     // the number of input pins we're using
const int INPUT_PINS[NUMBER_OF_INPUTS] = {2, 3, 5, 11}; // the actual input pins

//const IPAddress DEST_HOST(192, 168, 1, 255);
//const IPAddress DEST_HOST(10, 99, 30, 255); // BROADCAST TO "BCAST" VLAN
//const IPAddress DEST_HOST(10, 99, 30, 111); // UNICAST
const IPAddress DEST_HOST(10, 32, 1, 255); // TESTING @ HOME
const unsigned int DEST_PORT = 3310;

// Enter a MAC address and IP address for your controller below.
const byte MY_MAC[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xCA, 0xFE };
const IPAddress MY_IP(192, 168, 1, 123);

/******************************************************************************
 * RUNTIME GLOBAL VARIABLES
 *****************************************************************************/
Bounce * inputs = new Bounce[NUMBER_OF_INPUTS];
EthernetUDP Udp;
int status_light_state;
unsigned int blink_interval = 0;
unsigned long blink_started = 0;

/******************************************************************************
 SETUP FUNCTION
******************************************************************************/
void setup() {
  // set output pins mode
  pinMode(RELAY_CONTROL_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(STATUS_LED_PIN, OUTPUT);
  set_status_light(LOW);

  // set input pins mode, using debouncer library
  for (int pin = 0; pin < NUMBER_OF_INPUTS; pin++) {
    inputs[pin].attach(INPUT_PINS[pin], INPUT_PULLUP);
    inputs[pin].interval(DEBOUNCE_INTERVAL);
    inputs[pin].update();
  }

  // initialize serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect before continuing
  }
  debugMsg("DEBUG TERMINAL INITIATED. setup() continuing...");

  // initialize ethernet controller
  Ethernet.init(10);
  Ethernet.begin(MY_MAC, MY_IP);

  // Check for Ethernet hardware present
  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    debugMsg("FATAL: Ethernet shield was not found. Am I a joke to you?");
    while (true) {
      delay(1000); // do nothing, forever and ever and ever
    }
  }

  // initialize the udp library
  Udp.begin(DEST_PORT);

  // initialization done
  set_status_light(HIGH);
  debugMsg("Initialization completed. Number of inputs: " + (String) NUMBER_OF_INPUTS);
  debugMsg("My IP Address: " + (String) Ethernet.localIP());
}

/******************************************************************************
 MAIN LOOP
******************************************************************************/
void loop() {
  // wait for ethernet link
  while (Ethernet.linkStatus() != LinkON) {
    debugMsg("Ethernet cable is not connected.");
    flash_status_light(2);
  }

  // a test routine to cycle all indicators on and off
  if (ENABLE_DEBUG)
    test_indicators();

  // monitor input pins for state change
  for (int pin = 0; pin < NUMBER_OF_INPUTS; pin++) {
    inputs[pin].update();
    if (inputs[pin].rose() or inputs[pin].fell()) {
      blink_status_light(150); // milliseconds

      int currState = inputs[pin].read();
      debugMsg("State Change on pin " + (String) INPUT_PINS[pin]);
      debugMsg("  New State: " + (String) currState);

      // pin specific actions
      int state_to_send = (currState ? LOW : HIGH);
      switch (pin) {
        case 0: // "OnAir"
          digitalWrite(RELAY_CONTROL_PIN, state_to_send);
          send_udp_msg((String) "AIR1:"+(state_to_send ? "ON" : "OFF"));
          break;
        case 1: // "Phone"
          break;
        case 2: // N/C
          break;
        case 3: // "Doorbell" - this is a momentary input, so always send high
          state_to_send = HIGH;
          break;
      }

      // send the appropriate UDP packet
      set_indicator_state(pin, state_to_send);
    }
  }

  // always make sure the status led is on, unless we're still in a period of blinking
  if (status_light_state == 0 && millis() - blink_started > blink_interval)
    set_status_light(HIGH);
}

/******************************************************************************
 a test routine to cycle all indicators on and off
******************************************************************************/
void test_indicators() {
  debugMsg("Starting indicator test");
  flash_status_light(5);
  debugMsg("Turning indicators ON sequentially");
  for (int c = 0; c < NUMBER_OF_INPUTS; c++) {
    set_indicator_state(c+1, HIGH);
    delay(500);
  }
  delay(2000);
  debugMsg("Turning indicators OFF sequentially");
  for (int c = 0; c < NUMBER_OF_INPUTS; c++) {
    set_indicator_state(c+1, LOW);
    delay(500);
  }
  debugMsg("Indicator test completed.");
  delay(2000);
}

/******************************************************************************
 sets the state of an indicator on or off
******************************************************************************/
void set_indicator_state(int id, int state) {
  id++; // the OnAirScreen is 1-based, not 0-based
  String msg = "LED" + String(id) + ":";
  if (state)
    msg = msg + "ON";
  else
    msg = msg + "OFF";

  send_udp_msg(msg);
}

/******************************************************************************
 wrapper to handle the messy details of sending a udp packet
******************************************************************************/
void send_udp_msg(String msg) {
  msg.toUpperCase();
  debugMsg("Sending command: "+msg);

  // this function accepts a (String) but we need to pass
  // a (char) to the UDP library, so convert it here
  char m[16]; // none of our commands are (currently) > 16 chars. adjust in future as required.
  msg.toCharArray(m, sizeof(m));

  // send the packet onto the wire
  Udp.beginPacket(DEST_HOST, DEST_PORT);
  Udp.write(m);
  Udp.endPacket();
}

/******************************************************************************
 function to log messages for debugging to the serial console
******************************************************************************/
void debugMsg(String msg) {
  if (!ENABLE_DEBUG)
    return;

  Serial.println(msg);
  Serial.flush();
}

/******************************************************************************
 wrapper to control the status led
******************************************************************************/
void set_status_light(int state) {
  if (state) {
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(STATUS_LED_PIN, HIGH);
    status_light_state = 1;
  } else {
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(STATUS_LED_PIN, LOW);
    status_light_state = 0;
  }
}

/******************************************************************************
 blink the status led (blink == turn off momentarily)
******************************************************************************/
void blink_status_light(unsigned int ms) {
  set_status_light(LOW);
  blink_interval = ms;
  blink_started = millis();
}

/******************************************************************************
 flash the status led (flash == turn on momentarily)
 note this function uses delay(), so it will block other operations. it is to
 be used for indicating serious states (eg, ethernet cable unplugged)
******************************************************************************/
void flash_status_light(unsigned int cnt) {
  unsigned int flash_interval = 250;
  set_status_light(LOW);
  for (int x = 1; x <= cnt; x++) {
    set_status_light(HIGH);
    delay(flash_interval);
    set_status_light(LOW);
    delay(flash_interval);
  }
  delay(1000 - flash_interval);
}
